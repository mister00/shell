#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include "builders.h"

static const int WORD_INIT_LENGTH = 16;
static const int STRING_INIT_LENGTH = 16;
static const int ARGS_INIT_LENGTH = 8;
static const int REDIR_INIT_LENGTH = 2;
static const int PIPELINE_INIT_LENGTH = 4;
static const int LIST_INIT_LENGTH = 4;

static const char* CD = "cd";
static const char* EXIT = "exit";

static const char* SYNTAX_ERROR = "shell: syntax error";
static const char* PROGRAM_NAME = "shell";
static const char* ERR_TOO_MANY_ARGS = "too many arguments";

static const int ERRNO_NO_CHILD = 10;

static const int NUM_SPEC_SYMBOL = 5;
static const int MAX_LENGTH_SPEC_SYMBOL = 2;
static const char* spec_symbol[] = {"&", "|", ">>", ">", "<"};

int IsStrEqual(const char* a, const char* b){
    int i;
    for(i = 0; a[i] != '\0' || b[i] != '\0'; i++){
        if (a[i] != b[i]){
            return 0;
        }
    }
    return a[i] == b[i];
}

int IsSpecChar(int c){
    return c == '&' || c == '|' || c == '>' || c == '<';
}

enum Type CheckSpecSymbol(char* text){
    int type = -1;
    for (int i = 0; i <= NUM_SPEC_SYMBOL-1; i++){
        if (IsStrEqual(text, spec_symbol[i])){
            type = i;
        }
    }
    switch (type){
        case 0: return AMP;
        case 1: return STICK;
        case 2: return D_RIGHT_BR;
        case 3: return RIGHT_BR;
        case 4: return LEFT_BR;
        default: return NUL;
    }
}

int IsInWord(int c){
    return c != '\n' && c != '&' && c != '|' && c != '>' && c != '<' && c != ' ';
}

char* ReadWord(int* p_c){
    struct WordBuilder word;
    WordInit(&word, WORD_INIT_LENGTH);
    int quotes1 = 0;
    int quotes2 = 0;
    int backslash = 0;
    int backslash_in_quotes2 = 0;
    while (IsInWord(*p_c) || backslash || quotes1 || quotes2) {
        if (*p_c == '\'' && !quotes2 && !backslash) {
            quotes1 = !quotes1;
        } else if (*p_c == '"' && !quotes1 && !backslash) {
            quotes2 = !quotes2;
        } else if (*p_c == '\\' && !quotes1 && !backslash) {
            backslash = 1;
            if (quotes2){
                backslash_in_quotes2 = 1;
            }
        } else {
            if (backslash_in_quotes2 && *p_c != '\\' && *p_c != '\n' && *p_c != '"') {
                WordAppend(&word, '\\');
                backslash_in_quotes2 = 0;
            }
            if (!(*p_c == '\n' && backslash)){
                WordAppend(&word, *p_c);
            }
            backslash = 0;
        }
        *p_c = getchar();
    }
    return word.data;
}

enum Type ReadSpecSym(int* p_c){
    char* text = malloc((MAX_LENGTH_SPEC_SYMBOL + 2) * sizeof(char*));
    text[0] = '\0';
    int i = 0;
    while (IsSpecChar(*p_c) && *p_c != '\n'){
        text[i] = (char) *p_c;
        i++;
        text[i] = '\0';
        if (CheckSpecSymbol(text) == NUL) {
            text[i-1] = '\0';
            break;
        }
        *p_c = getchar();
    }
    return CheckSpecSymbol(text);
}

struct Token* ReadString(){
    int c = getchar();
    struct StringBuilder string;
    StringInit(&string, STRING_INIT_LENGTH);
    while (c == ' '){
        c = getchar();
    }
    struct Token token;
    while (c != '\n'){
        if (!IsSpecChar(c)) {
            token.text = ReadWord(&c);
            token.type = WORD;
        } else {
            token.text = NULL;
            token.type = ReadSpecSym(&c);
        }
        StringAppend(&string, token);
        while (c == ' '){
            c = getchar();
        }
    }
    return string.data;
}

struct Command* ReadCommand(struct Token* string, int num_words, int* p_err){
    struct Command* command = (struct Command*) malloc(sizeof(struct Command));
    struct ArrBuilder args;
    struct ArrBuilder input;
    struct ArrBuilder output;
    struct ArrBuilder output_append;
    ArrInit(&args, ARGS_INIT_LENGTH);
    ArrInit(&input, REDIR_INIT_LENGTH);
    ArrInit(&output, REDIR_INIT_LENGTH);
    ArrInit(&output_append, REDIR_INIT_LENGTH);
    enum Type mode = NUL;
    for (int i = 0; i < num_words && *p_err == 0; i++){
        if (string[i].type != WORD) {
            if (mode == NUL) {
                mode = string[i].type;
            } else {
                *p_err = 1;
            }
        } else {
            switch (mode){
                case NUL:
                    ArrAppend(&args, string[i].text);
                    break;
                case LEFT_BR:
                    ArrAppend(&input, string[i].text);
                    break;
                case RIGHT_BR:
                    ArrAppend(&output, string[i].text);
                    break;
                case D_RIGHT_BR:
                    ArrAppend(&output_append, string[i].text);
                    break;
                default:;
            }
            mode = NUL;
        }
    }
    if (mode != NUL || args.data[0] == NULL || (input.data[0] != NULL && input.data[1] != NULL)
        || (output.data[0] != NULL && output.data[1] != NULL)
        || (output_append.data[0] != NULL && output_append.data[1] != NULL)
        || (output.data[0] != NULL && output_append.data[0] != NULL))
    {
        *p_err = 1;
    }
    command->args = args.data;
    command->input = input.data;
    command->output = output.data;
    command->output_append = output_append.data;
    return command;
}

struct Command** ReadPipeline(struct Token* string, int num_words, int* p_err){
    struct PipelineBuilder pipeline;
    PipelineInit(&pipeline, PIPELINE_INIT_LENGTH);
    int right = 0;
    int left = 0;
    while (right <= num_words && *p_err == 0){
        if (right == num_words || string[right].type == STICK){
            if (right > left){
                PipelineAppend(&pipeline, ReadCommand(string + left, right - left, p_err));
                left = right + 1;
            } else {
                *p_err = 1;
            }
        }
        right++;
    }
    return pipeline.data;
}

struct List{
    struct Command*** pipelines;
    int last_amp;
};

struct List ReadList(struct Token* string, int* p_err){
    struct ListBuilder list;
    ListInit(&list, LIST_INIT_LENGTH);
    struct List list_s;
    list_s.last_amp = 0;
    int right = 1;
    int left = 0;
    while (string[right-1].type != NUL && *p_err == 0) {
        if (string[right].type == NUL || string[right].type == AMP){
            if (right > left){
                ListAppend(&list, ReadPipeline(string + left, right - left, p_err));
                left = right + 1;
            } else if (string[right].type == AMP){
                *p_err = 1;
            } else {
                list_s.last_amp = 1;
            }
        }
        right++;
    }
    list_s.pipelines = list.data;
    return list_s;
}

int IsDirective(char* str){
    return IsStrEqual(str, CD) || IsStrEqual(str, EXIT);
}

void CheckError(int return_code, const char* name){
    if (return_code == -1){
        perror(name);
        exit(-1);
    }
}

int ExecDirective(struct Command* command){
    int return_code;
    if (IsStrEqual(command->args[0], EXIT)){
        return 1;
    } else if (IsStrEqual(command->args[0], CD)){
        if (command->args[1] != NULL) {
            if (command->args[2] != NULL){
                fprintf(stderr, "%s: %s\n", CD, ERR_TOO_MANY_ARGS);
            } else {
                return_code = chdir(command->args[1]);
                if (return_code == -1) {
                    perror(command->args[1]);
                    return -1;
                }
            }
        } else {
            return_code = chdir(getenv("HOME"));
            if (return_code == -1){
                perror(PROGRAM_NAME);
                return -1;
            }
        }
    }
    return 0;
}

void CloseFiles(int* inputs, int* outputs){
    for (int i = 0; inputs[i] != -1; i++){
        CheckError(close(inputs[i]), PROGRAM_NAME);
        CheckError(close(outputs[i]), PROGRAM_NAME);
    }
}

int ExecCommand(struct Command* command, int* inputs, int* outputs, int num){
    if (IsDirective(command->args[0])){
        return ExecDirective(command);
    }
    pid_t cpid;
    if ((cpid = fork()) != 0){
        CheckError(cpid, PROGRAM_NAME);
        return cpid;
    }
    if (inputs[num] != 0){
        CheckError(dup2(inputs[num], 0), PROGRAM_NAME);
    }
    if (outputs[num] != 1) {
        CheckError(dup2(outputs[num], 1), PROGRAM_NAME);
    }
    CloseFiles(inputs, outputs);
    CheckError(execvp(command->args[0], command->args), command->args[0]);
    return 0;
}

int FormPipelineIO(struct Command** pipeline, int** p_inputs, int** p_outputs){
    int size = 0;
    int pipes[2];
    for (; pipeline[size] != NULL; size++);
    *p_inputs = malloc((size + 1) * sizeof(int));
    *p_outputs = malloc((size + 1) * sizeof(int));
    (*p_inputs)[size] = -1;
    (*p_outputs)[size] = -1;
    for (int i = 0; i < size; i++){
        (*p_inputs)[i] = 0;
        (*p_outputs)[i] = 0;
        if (pipeline[i]->input[0] != NULL){
            (*p_inputs)[i] = open(pipeline[i]->input[0], O_RDONLY);
            if ((*p_inputs)[i] == -1){
                perror(pipeline[i]->input[0]);
                return -1;
            }
        } else if (i == 0){
            (*p_inputs)[0] = dup(0);
            if ((*p_inputs)[i] == -1){
                perror(PROGRAM_NAME);
                return -1;
            }
        }
        if (pipeline[i]->output_append[0] != NULL){
            (*p_outputs)[i] = open(pipeline[i]->output_append[0], O_CREAT | O_WRONLY | O_APPEND, S_IRWXU);
            if ((*p_outputs)[i] == -1){
                perror(pipeline[i]->output_append[0]);
                return -1;
            }
        } else if (pipeline[i]->output[0] != NULL){
            (*p_outputs)[i] = open(pipeline[i]->output[0], O_CREAT | O_WRONLY | O_TRUNC, S_IRWXU);
            if ((*p_outputs)[i] == -1){
                perror(pipeline[i]->output[0]);
                return -1;
            }
        } else if (i == size-1){
            (*p_outputs)[i] = dup(1);
            if ((*p_outputs)[i] == -1){
                perror(PROGRAM_NAME);
                return -1;
            }
        }
    }
    for (int i = 0; i < size-1; i++){
        if ((*p_outputs)[i] == 0 && (*p_inputs)[i+1] == 0){
            if (pipe(pipes) == -1){
                perror(PROGRAM_NAME);
                return - 1;
            }
            (*p_outputs)[i] = pipes[1];
            (*p_inputs)[i+1] = pipes[0];
        } else if ((*p_outputs)[i] == 0){
            (*p_outputs)[i] = open("/dev/null", O_WRONLY);
            if ((*p_outputs)[i] == -1){
                perror(pipeline[i]->output[0]);
                return -1;
            }
        } else if ((*p_inputs)[i+1] == 0){
            (*p_inputs)[i+1] = open("/dev/null", O_RDONLY);
            if ((*p_inputs)[i] == -1){
                perror(PROGRAM_NAME);
                return -1;
            }
        }
    }
    return 0;
}

int ExecPipeline(struct Command** pipeline, int bg_mode){
    pid_t cpid = 0;
    int return_code = 0;
    int forked = 0;
    if (pipeline[1] != NULL || bg_mode) {
        forked = 1;
        if ((cpid = fork()) != 0) {
            CheckError(cpid, PROGRAM_NAME);
            if (bg_mode == 1) {
                return cpid;
            } else {
                CheckError(waitpid(cpid, &return_code, 0), PROGRAM_NAME);
                if (return_code == 0) {
                    return 0;
                } else {
                    return -1;
                }
            }
        }
    }
    int* inputs = NULL;
    int* outputs = NULL;
    if (FormPipelineIO(pipeline, &inputs, &outputs) == -1){
        if (forked){
            exit(-1);
        } else {
            return -1;
        }
    }
    for (int i = 0; pipeline[i] != NULL; i++){
        return_code = ExecCommand(pipeline[i], inputs, outputs, i);
    }
    CloseFiles(inputs, outputs);
    if (return_code > 1 && !forked){
        CheckError(waitpid(return_code, &return_code, 0), PROGRAM_NAME);
    } else {
        while(wait(&return_code) > 0);
        if (errno != ERRNO_NO_CHILD){
            perror(PROGRAM_NAME);
            exit(-1);
        }
    }
    if (!forked){
        return return_code;
    } else {
        if (return_code == 0) {
            exit(0);
        } else {
            exit(-1);
        }
    }
}

int ExecList (struct List list){
    int i = 0;
    if (list.pipelines[0] == NULL){
        return 0;
    }
    for (; list.pipelines[i+1] != NULL; i++){
        ExecPipeline(list.pipelines[i], 1);
    }
    return ExecPipeline(list.pipelines[i], list.last_amp);
}

void PrintGreeting(){
    char* cwd = getcwd(NULL, PATH_MAX);
    printf("%s$ ", cwd);
    free(cwd);
    fflush(stdout);
}

void ChildWait(){
    int return_code;
    do {
        return_code = waitpid(-1, NULL, WNOHANG);
        if (return_code == -1 && errno != ERRNO_NO_CHILD){
            perror(PROGRAM_NAME);
        }
    } while (return_code > 0);
}

int main(){
    int err;
    int return_code = 0;
    struct Token* string;
    struct List list;
    while (return_code != 1){
        ChildWait();
        PrintGreeting();
        err = 0;
        string = ReadString();
        list = ReadList(string, &err);
        if (err == 0){
            return_code = ExecList(list);
        } else {
            fprintf(stderr, "%s\n", SYNTAX_ERROR);
        }
        StringDestroy(string);
        ListDestroy(list.pipelines);
    }
    return 0;
}
