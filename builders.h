struct WordBuilder{
    char* data;
    int size;
    int len;
};

void WordInit(struct WordBuilder* p_word, int size);
void WordAppend(struct WordBuilder* p_word, int c);
void WordDestroy(char* word);

enum Type{
    NUL, WORD, AMP, STICK, LEFT_BR, RIGHT_BR, D_RIGHT_BR
};

struct Token{
    char* text;
    enum Type type;
};

struct StringBuilder{
    struct Token* data;
    int size;
    int len;
};

void StringInit(struct StringBuilder* p_arr, int size);
void StringAppend(struct StringBuilder* p_arr, struct Token token);
void StringDestroy(struct Token* string);

struct ArrBuilder{
    char** data;
    int size;
    int len;
};

void ArrInit(struct ArrBuilder* p_arr, int size);
void ArrAppend(struct ArrBuilder* p_arr, char* word);

struct Command{
    char** args;
    char** input;
    char** output;
    char** output_append;
};

void CommandDestroy(struct Command* p_command);

struct PipelineBuilder{
    struct Command** data;
    int size;
    int len;
};

void PipelineInit(struct PipelineBuilder* p_pipeline, int size);
void PipelineAppend(struct PipelineBuilder* p_pipeline, struct Command* p_command);
void PipelineDestroy(struct Command** pipeline);

struct ListBuilder{
    struct Command*** data;
    int size;
    int len;
};

void ListInit(struct ListBuilder* p_list, int size);
void ListAppend(struct ListBuilder* p_list, struct Command** p_pipeline);
void ListDestroy(struct Command*** list);
