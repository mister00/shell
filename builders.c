#include <stdio.h>
#include <stdlib.h>
#include "builders.h"

static const char* PROGRAM_NAME = "shell";

void WordInit(struct WordBuilder* p_word, int size){
    p_word->data = malloc (size * sizeof(char));
    if (p_word->data == NULL){
        perror("shell");
        exit(-1);
    }
    p_word->len = 0;
    p_word->size = size;
    p_word->data[0] = '\n';
}

void WordExtend(struct WordBuilder* p_word){
    char* new_word = malloc(p_word->size * 2 * sizeof(char));
    if (new_word == NULL){
        perror(PROGRAM_NAME);
        exit(-1);
    }
    for (int i = 0; i < p_word->size; i++){
        new_word[i] = p_word->data[i];
    }
    p_word->size = p_word->size * 2;
    free(p_word->data);
    p_word->data = new_word;
}

void WordAppend(struct WordBuilder* p_word, int c){
    p_word->data[p_word->len] = (char) c;
    p_word->len++;
    if (p_word->len == p_word->size) {
        WordExtend(p_word);
    }
    p_word->data[p_word->len] = '\0';
}

void WordDestroy(char* word){
    free(word);
}

void StringInit(struct StringBuilder* p_string, int size){
    p_string->data = malloc (size * sizeof(struct Token));
    if (p_string->data == NULL){
        perror(PROGRAM_NAME);
        exit(-1);
    }
    p_string->len = 0;
    p_string->size = size;
    p_string->data[0].type = NUL;
}

void StringExtend(struct StringBuilder* p_string){
    struct Token* new_string = malloc(p_string->size * 2 * sizeof(struct Token));
    if (new_string == NULL){
        perror(PROGRAM_NAME);
        exit(-1);
    }
    for (int i = 0; i < p_string->size; i++){
        new_string[i] = (p_string->data)[i];
    }
    p_string->size = p_string->size * 2;
    free(p_string->data);
    p_string->data = new_string;
}

void StringAppend(struct StringBuilder* p_string, struct Token token){
    p_string->data[p_string->len].text = token.text;
    p_string->data[p_string->len].type = token.type;
    p_string->len++;
    if (p_string->len == p_string->size) {
        StringExtend(p_string);
    }
    p_string->data[p_string->len].type = NUL;
}

void StringDestroy(struct Token* string){
    for(int i=0; string[i].type != NUL; i++){
        if (string[i].text != NULL){
            WordDestroy(string[i].text);
        }
    }
    free(string);
}

void ArrInit(struct ArrBuilder* p_arr, int size){
    p_arr->data = malloc (size * sizeof(char*));
    if (p_arr->data == NULL){
        perror(PROGRAM_NAME);
        exit(-1);
    }
    p_arr->len = 0;
    p_arr->size = size;
    p_arr->data[0] = NULL;
}

void ArrExtend(struct ArrBuilder* p_arr){
    char** new_arr = malloc(p_arr->size * 2 * sizeof(char*));
    if (new_arr == NULL){
        perror(PROGRAM_NAME);
        exit(-1);
    }
    for (int i = 0; i < p_arr->size; i++){
        new_arr[i] = (p_arr->data)[i];
    }
    p_arr->size = p_arr->size * 2;
    free(p_arr->data);
    p_arr->data = new_arr;
}

void ArrAppend(struct ArrBuilder* p_arr, char* word){
    p_arr->data[p_arr->len] = word;
    p_arr->len++;
    if (p_arr->len == p_arr->size) {
        ArrExtend(p_arr);
    }
    p_arr->data[p_arr->len] = NULL;
}

void CommandDestroy(struct Command* p_command){
    free(p_command->args);
    free(p_command->input);
    free(p_command->output);
    free(p_command->output_append);
    free(p_command);
}

void PipelineInit(struct PipelineBuilder* p_pipeline, int size){
    p_pipeline->data = malloc (size * sizeof(struct Command*));
    if (p_pipeline->data == NULL){
        perror(PROGRAM_NAME);
        exit(-1);
    }
    p_pipeline->len = 0;
    p_pipeline->size = size;
    p_pipeline->data[0] = NULL;
}

void PipelineExtend(struct PipelineBuilder* p_pipeline){
    struct Command** new_pipeline = malloc(p_pipeline->size * 2 * sizeof(struct Command));
    if (new_pipeline == NULL){
        perror(PROGRAM_NAME);
        exit(-1);
    }
    for (int i = 0; i < p_pipeline->size; i++){
        new_pipeline[i] = (p_pipeline->data)[i];
    }
    p_pipeline->size = p_pipeline->size * 2;
    free(p_pipeline->data);
    p_pipeline->data = new_pipeline;
}

void PipelineAppend(struct PipelineBuilder* p_pipeline, struct Command* p_command){
    p_pipeline->data[p_pipeline->len] = p_command;
    p_pipeline->len++;
    if (p_pipeline->len == p_pipeline->size) {
        PipelineExtend(p_pipeline);
    }
    p_pipeline->data[p_pipeline->len] = NULL;
}

void PipelineDestroy(struct Command** pipeline){
    for(int i=0; pipeline[i] != NULL; i++){
        CommandDestroy(pipeline[i]);
    }
    free(pipeline);
}

void ListInit(struct ListBuilder* p_list, int size){
    p_list->data = malloc (size * sizeof(struct Command**));
    if (p_list->data == NULL){
        perror(PROGRAM_NAME);
        exit(-1);
    }
    p_list->len = 0;
    p_list->size = size;
    p_list->data[0] = NULL;
}

void ListExtend(struct ListBuilder* p_list){
    struct Command*** new_list = malloc(p_list->size * 2 * sizeof(struct Command));
    if (new_list == NULL){
        perror(PROGRAM_NAME);
        exit(-1);
    }
    for (int i = 0; i < p_list->size; i++){
        new_list[i] = (p_list->data)[i];
    }
    p_list->size = p_list->size * 2;
    free(p_list->data);
    p_list->data = new_list;
}

void ListAppend(struct ListBuilder* p_list, struct Command** p_command){
    p_list->data[p_list->len] = p_command;
    p_list->len++;
    if (p_list->len == p_list->size) {
        ListExtend(p_list);
    }
    p_list->data[p_list->len] = NULL;
}

void ListDestroy(struct Command*** list){
    for(int i=0; list[i] != NULL; i++){
        PipelineDestroy(list[i]);
    }
    free(list);
}